import React from 'react'
import Headers from './components/Headers'
import ProductList from './components/ProductList'

const App = () => {
  return (
    <div>
      <Headers></Headers>
      <ProductList></ProductList>
    </div>
  )
}

export default App
