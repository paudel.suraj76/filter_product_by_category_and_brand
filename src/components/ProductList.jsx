import axios from 'axios'
import React, { useEffect, useState } from 'react'

const ProductList = () => {

  let [product, setProduct] = useState([])
  let [category, setCategory] = useState([])
  let [brand, setBrand] = useState([])
  let [selectedCategory, setSelectedCategory] = useState("")
  let [selectedBrand, setSelectedBrand] = useState("")

  let obj = {
    url: 'https://dummyjson.com/products',
    method: 'GET',
  }

  let fetchProduct = async () => {
    let result = await axios(obj)
    setProduct(result.data.products)
  }

  useEffect(() => {
    fetchProduct()
  }, [])
  useEffect(() => {
    setCategory(
      [...new Set(product.map((item, i) => {
        return item.category
      }))]
    )
  }, [product])



useEffect(()=>{
setSelectedBrand("")
  if(selectedCategory){
    let filteredByBrand=product.filter(item=>item.category===selectedCategory)
   setBrand(
    [...new Set(filteredByBrand.map((item,i)=>{
      return item.brand
    }))]
   )
    
  }
},[product,selectedCategory])

let filterProductByCategory=selectedCategory?product.filter(item=>item.category===selectedCategory):product

  let filterProductByBrand = selectedBrand !== '' ? filterProductByCategory.filter(item => item.brand === selectedBrand): filterProductByCategory;


  return (
    <div>
      <div className='bg-gray-700 text-white mr-2 ml-2 mt-1 p-2 flex justify-start'>
        <div className='pb-2 pr-2'>

          <select value={selectedCategory} onChange={(e) => { setSelectedCategory(e.target.value) }} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-4 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
            <option value=""  disabled={true}>Choose a category</option>
            {
              category.map((item, i) => {
                return <option key={i} value={item}>{item}</option>
              })
            }
          </select>
        </div>

        <div>
          <select value={selectedBrand} onChange={(e) => {
            setSelectedBrand(e.target.value)
          }} id="countries" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-4 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
            <option value="" disabled={true}>Choose a brand</option>
            {
              brand.map((item, i) => {
                return <option key={i} value={item}>{item}</option>
              })
            }

          </select>
        </div>
      </div>

      <div className='bg-slate-500 mr-2 ml-2 mt-1 p-1 flex '>
        <h2 className='ml-12 border-r-2 pr-3 font-extrabold text-white max-w-fit p-1 text-lg'>All Products</h2>
        <p className='ml-12 pr-3 font-bold text-white max-w-fit p-1 text-lg'>{selectedCategory && selectedBrand ? `Your product under ${selectedCategory}` : null}</p>
      </div>

      <div className="p-5 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-5">
        {/* cards */}
        {
          filterProductByBrand.map((item, index) => (
            <div key={index} className="rounded overflow-hidden shadow-lg bg-amber-600 min-w-fit">
              <img className="w-full h-44" src={item.thumbnail} alt="Mountain"></img>
              <div className="px-6 py-4">
                <div className="font-bold text-2xl mb-2">{item.brand}</div>
                <div className="font-semibold text-lg mb-2">{item.title}</div>
                <p className="text-gray-200 text-base">{item.description}</p>
              </div>
              <div className="p-1 h-2/4">
                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-00 mr-2 mb-2">Price: {item.price}</span>
                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Discount: {item.discountPercentage}%</span>
                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Rating: {item.rating}</span>
                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Stock: {item.stock}</span>
              </div>
            </div>
          ))}
      </div>
    </div>

  )
}

export default ProductList
